#!/bin/bash

cd /apps/poc/
if [ -f app"$1".pid ]; then
        PID=`cat app"$1".pid`
        echo "kill pid $PID"
        kill -9 $PID
        else
        echo "no_pid found"
fi
rm -f app"$1".pid
