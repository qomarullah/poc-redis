package com.tsel.controllers;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.Duration;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xerial.snappy.Snappy;

import com.tsel.cache.JedisSentinel;
import com.tsel.models.Result;
import com.tsel.routers.Router;
import com.tsel.utils.Utils;

import io.micrometer.core.instrument.Timer;

public class Cache {
	private final static Logger log = LogManager.getLogger(Router.class);
	
	
	public Cache() {
		
	}

	public Result cacheSetSize(String key, String value, int duration, Timer timer) {
		long start=System.currentTimeMillis();
		
		Result result = new Result();
		result.setStatus("00");
		result.setMessage("Success");
		
		String newKey = Utils.generateTxid("KEY", "628118003585");
		String newValue = StringUtils.repeat("*", Integer.parseInt(value));
		
		try {	
			JedisSentinel.set(newKey,newValue,duration);
		} catch (Exception e) {
			result.setStatus("08");
			result.setMessage("Error cache");
			return result;
		}
		Duration rt=Duration.ofMillis(System.currentTimeMillis()-start);
		timer.record(rt);
		
		return result;
	}
	
	public Result cacheSetSizeCompress(String key, String value, int duration, Timer timer) {
		long start=System.currentTimeMillis();
		
		Result result = new Result();
		result.setStatus("00");
		result.setMessage("Success");
		
		String newKey = Utils.generateTxid("KEY", "628118003585");
		String newValue = StringUtils.repeat("*", Integer.parseInt(value));
	
		try {	
			byte[] keyByte=newKey.getBytes();
			byte[] compressed = Snappy.compress(newValue.getBytes("UTF-8"));		
			JedisSentinel.setBinary(keyByte,compressed,duration);
		} catch (Exception e) {
			result.setStatus("08");
			result.setMessage("Error cache");
			return result;
		}
		Duration rt=Duration.ofMillis(System.currentTimeMillis()-start);
		timer.record(rt);
		return result;
	}
	
	public Result cacheRemove(String key) {
		Result result = new Result();
		result.setStatus("00");
		result.setMessage("Success");
		
		try {	
			JedisSentinel.remove(key);
		} catch (Exception e) {
			result.setStatus("08");
			result.setMessage("Error cache");
			return result;
		}
		return result;
	}
	public Result cacheSet(String key, String value, int duration) {
		Result result = new Result();
		result.setStatus("00");
		result.setMessage("Success");
		
		try {	
			JedisSentinel.set(key,value,duration);
		} catch (Exception e) {
			result.setStatus("08");
			result.setMessage("Error cache");
			return result;
		}
		return result;
	}
	public Result cacheFlush() {
		Result result = new Result();
		result.setStatus("00");
		result.setMessage("Success");
		
		try {	
			JedisSentinel.flush();
		} catch (Exception e) {
			result.setStatus("08");
			result.setMessage("Error cache");
			return result;
		}
		return result;
	}
	public Result cacheGet(String key) {
		Result result = new Result();
		result.setStatus("00");
		result.setMessage("Success");
		
		try {	
			String data = JedisSentinel.get(key);
			//if(data==null)result.setData("null");
			//else 
			result.setData(data);
			
		} catch (Exception e) {
			result.setStatus("08");
			result.setMessage("Error cache");
			return result;
		}
		return result;
	}

}
