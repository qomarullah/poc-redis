package com.tsel.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResultDb implements Serializable{
	
    public String status = "";
    public String desc="";
    public int count = 0;
    public List<Object> results = new ArrayList<>();

}