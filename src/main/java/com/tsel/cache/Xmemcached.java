package com.tsel.cache;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.tsel.poc.App;

import net.rubyeye.xmemcached.KeyIterator;
import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.MemcachedClientBuilder;
import net.rubyeye.xmemcached.XMemcachedClient;
import net.rubyeye.xmemcached.XMemcachedClientBuilder;
import net.rubyeye.xmemcached.exception.MemcachedException;
import net.rubyeye.xmemcached.impl.KetamaMemcachedSessionLocator;
import net.rubyeye.xmemcached.utils.AddrUtil;



public class Xmemcached{
	
	public static Logger log = LogManager.getLogger(Xmemcached.class);
	
	private static MemcachedClient mcc;
	public static String serverID;
	
	public Xmemcached(String id) {
		serverID=id;
		init();
	}
	
	public Xmemcached() {
		serverID=App.serverID;
		init();
	}
	public static void restart() {
		mcc = null;
		init();
	}
	
	public static void flush() {
		try {
			mcc.flushAll();
		} catch (TimeoutException | InterruptedException | MemcachedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.debug("error flush:"+e.getMessage());
		}
	}

	public static void init() {
		log.info("Try initializing memcached...");
		
		serverID=App.serverID;
		
		try {
			if (mcc == null) {
					String memcachedServer = App.memcachedServer;
					String memcachedServerPool = App.memcachedServerPool;
						
					log.debug("load memcached server|"+memcachedServer);
					
					MemcachedClientBuilder builder = new XMemcachedClientBuilder(AddrUtil.getAddresses(memcachedServer));
					builder.setConnectionPoolSize(Integer.parseInt(memcachedServerPool));
					builder.setSessionLocator(new KetamaMemcachedSessionLocator());
				    mcc = builder.build();
				    
				}
			
		} catch (Exception e) {
			log.error("Error init memcached:" + e);
			e.printStackTrace();
		}
	}


	public static boolean set(String key, String value, int expiry) {
		key=key.replaceAll(" ","_");
		
		boolean ret = false;
		try {
			if (mcc == null) {
				init();
			}
			ret = mcc.set(key, expiry, value);
			log.debug("Memcached|set:"+key+"|"+expiry+"|"+value+"="+ret);
		} catch (Exception e) {
			
			log.error("Error set memcached element:" + e);
			if (mcc == null) init();
			
		}
		
		return ret;
		
		
	}


	public static String get(String key) {
		
		Object val = null;
		key=key.replaceAll(" ","_");
		if (mcc == null) {
			init();
		}
		try {
			val = mcc.get(key);
		} catch (Exception e) {
			log.error("Error get memcached element:" + e);
		}
		
		log.debug("Memcached|Get:"+key+"="+val);
		
		if (val == null) {
			return null;
		} else {
			return val.toString();
		}
		
	}


	public static Map<String, Object> getbulk(ArrayList<String> keybulk) {
		
		Map<String, Object> valbulk = null;
		
		try {
			if (mcc == null) {
				init();
			}
			valbulk = mcc.get(keybulk);
			
			log.debug(String.valueOf(valbulk));
		} catch (Exception e) {
			log.error("Error get memcached element:" + e);
		}
		
		log.debug("Memcached|GetBulk:"+keybulk+"="+valbulk);
		
		if (valbulk == null) {
			return null;
		} else {
			return valbulk;
		}
		
	}
	public static String get(String key, int to) {
		long start = System.currentTimeMillis();
		
		if(to==0)
			return get(key);
		
		key=key.replaceAll(" ","_");
		
		Object val = null;
		try {
			if (mcc == null) {
				init();
			}
			val = mcc.get(key,to);
		
			
		}catch(TimeoutException e) {
			log.debug("Memcached|Get-timeout:"+key+"="+val);
			
		} catch (Exception e) {
			log.error("Error get memcached element:" + e);
		}
		log.debug("Memcached|Get:"+key+"="+val);
		
		long time = System.currentTimeMillis() - start;
		log.info("Memcached|Get|"+time+"|"+key+"="+val);
		
		if (val == null) {
			return null;
		} else {
			return val.toString();
		}
		
		
	}
	public static void remove(String key) {

		try {
			key=key.replaceAll(" ","_");
			if (mcc == null) {
				init();
			}
			mcc.delete(key);
		} catch (TimeoutException | InterruptedException | MemcachedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.debug("error delete element:"+e.getMessage());
		}
	}
	
	public static void main(String[] args){
		System.out.println("bismillah");
		String hostname="localhost";
	
		MemcachedClient c;
		try {
			long start = System.currentTimeMillis();
			c = new XMemcachedClient(new InetSocketAddress(hostname, 11211));
			
			/*MemcachedClientBuilder builder = new XMemcachedClientBuilder(AddrUtil.getAddresses("localhost:11211"));
		    builder.setSessionLocator(new KetamaMemcachedSessionLocator());
		    MemcachedClient client=builder.build();
		    c = builder.build();
		    */
		    // Store a value (async) for one hour
			try {
				c.set("key", 3600, "data");
			} catch (TimeoutException | InterruptedException
					| MemcachedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Retrieve a value (synchronously).
			Object myObject = null;
			try {
				myObject = c.get("key");
			} catch (TimeoutException | InterruptedException
					| MemcachedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("key:"+myObject.toString());
			long time = System.currentTimeMillis() - start;
			System.out.println(String.valueOf(time));
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
	}
}
