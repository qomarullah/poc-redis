package com.tsel.cache;
import org.redisson.Redisson;
import org.redisson.api.RBucket;
import org.redisson.api.RLock;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
/**
 * Redis Sentinel Java example
 *
 */
public class RedissonTest 
{
    public static void main( String[] args )
    {
        Config config = new Config();
        config.useSentinelServers()
              .addSentinelAddress("redis://52.74.222.213:26379,redis://52.74.222.213:26380,redis://52.74.222.213:26381")
              .setMasterName("master");
        RedissonClient redisson = Redisson.create(config);
        // perform operations
        // implements java.util.concurrent.ConcurrentMap
        RMap<String, String> map = redisson.getMap("simpleMap");
        map.put("mapKey", "This is a map value");
        String mapValue = map.get("mapKey");
        System.out.println("stored map value: " + mapValue);
        // implements java.util.concurrent.locks.Lock
        RLock lock = redisson.getLock("simpleLock");
        lock.lock();
        try {
           // do some actions
        } finally {
           lock.unlock();
        }
        redisson.shutdown();
    }
}