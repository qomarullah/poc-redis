package com.tsel.cache;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.SecureRandom;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.tsel.poc.App;

import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.XMemcachedClient;
import net.rubyeye.xmemcached.exception.MemcachedException;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisSentinelPool;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class JedisSentinel {

	public static Logger log = LogManager.getLogger(JedisCluster.class);

	private static JedisSentinelPool pool;
	public static String serverID;
	private static final String MASTER_NAME = "master";
    public static final String PASSWORD = "";
    private static Set sentinels;


	public JedisSentinel() {
		serverID = App.serverID;
		init();
	}

	public static void restart() {
		pool = null;
		init();
	}
	

	public static void init() {
		log.info("Try initializing jedis...======================");
		serverID = App.serverID;

		try {
			if (pool == null) {
				
				log.debug("load jedis cluster|" + App.redisServer);
				String[] redisServer = App.redisServer.split(",");
				String[] redisConfig = App.redisConfig.split("\\|");
				
				sentinels = new HashSet();
			    for (int i = 0; i < redisServer.length; i++) {
					 sentinels.add(redisServer[i]);
			    } 
			    
			    GenericObjectPoolConfig config = new GenericObjectPoolConfig();
			    config.setMaxTotal(Integer.parseInt(redisConfig[0]));
				config.setMaxIdle(Integer.parseInt(redisConfig[1]));
				config.setMinIdle(Integer.parseInt(redisConfig[2]));
				
				//config.setTestOnBorrow(true);
				//config.setTestOnReturn(true);
				//config.setTestWhileIdle(true);
				//config.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
				//config.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
				//config.setNumTestsPerEvictionRun(3);
				
				config.setBlockWhenExhausted(false);
				int timeout=Integer.parseInt(redisConfig[3]);
			    if(App.redisPassword.equals("")) App.redisPassword=null;
			    
				pool = new JedisSentinelPool(App.redisMaster, sentinels, config, timeout, App.redisPassword);
			    log.debug("connect-jedis|" + pool.getResource().set("ping", "ok"));
			}

		} catch (Exception e) {
			log.error("Error init jedis:" + e);
			e.printStackTrace();
		}
	}

	public static boolean set(String key, String value, int expiry) {
		key = key.replaceAll(" ", "_");
		boolean ret = false;
		
		if (pool == null) {
			init();
		}
		Jedis jedis=pool.getResource();
		
		try {
			
			String resp = null;
			if (expiry > 0) {
				resp = jedis.setex(key, expiry, value);
			} else {
				resp = jedis.set(key, value);
				
			}
			if (resp.equals("OK"))ret=true;
			
			log.debug("Redis|set|" + key + "|" + expiry + "|" + value + "=" + resp);
			
		} catch (Exception e) {
			
			log.error("Error set Redis element:" + e);
			if (pool == null)
				init();

		}finally {
			try {
				if (jedis!=null)jedis.close();
			} catch (Exception e2) {
				// TODO: handle exception
				log.debug("Redis|close:"+e2.getMessage());
			}
		}

		return ret;

	}
	public static boolean setBinary(byte[] key, byte[] value, int expiry) {
		boolean ret = false;
		
		if (pool == null) {
			init();
		}
		Jedis jedis=pool.getResource();
		
		try {
			
			String resp = null;
			if (expiry > 0) {
				resp = jedis.setex(key, expiry, value);
			} else {
				resp = jedis.set(key, value);
				
			}
			if (resp.equals("OK"))ret=true;
			
			log.debug("Redis|set|" + key + "|" + expiry + "|" + value + "=" + resp);
			
		} catch (Exception e) {
			
			log.error("Error set Redis element:" + e);
			if (pool == null)
				init();

		}finally {
			try {
				if (jedis!=null)jedis.close();
			} catch (Exception e2) {
				// TODO: handle exception
				log.debug("Redis|close:"+e2.getMessage());
			}
		}

		return ret;

	}

	public static String get(String key) {

		Object val = null;
		key = key.replaceAll(" ", "_");
		
		if (pool == null) {
			init();
		}
		Jedis jedis=pool.getResource();
		
		try {
			val = jedis.get(key);
		} catch (Exception e) {
			log.error("Error get Redis element:" + e);
			
		}finally {
			try {
				if (jedis!=null)jedis.close();
			} catch (Exception e2) {
				// TODO: handle exception
				log.debug("Redis|close:"+e2.getMessage());
			}
		}

		log.debug("Redis|get|" + key + "=" + val);

		if (val == null) {
			return null;
		} else {
			return val.toString();
		}
	}
	public static void flush() {

		if (pool == null) {
			init();
		}
		Jedis jedis=pool.getResource();
		
		try {
			jedis.flushAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.debug("error delete element:"+e.getMessage());
		}
	}
	public static void remove(String key) {

		key = key.replaceAll(" ", "_");
		
		if (pool == null) {
			init();
		}
		Jedis jedis=pool.getResource();
		
		try {
			jedis.del(key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.debug("error delete element:"+e.getMessage());
		}
	}

	public static void initLocal() {

		System.out.println("Try initializing jedis...");
		String redisServerLocal = "52.74.222.213:26379,52.74.222.213:26380,52.74.222.213:26381";

		try {
			if (pool == null) {

				System.out.println("load jedis cluster|" + redisServerLocal);
				String[] redisServer = redisServerLocal.split(",");

				sentinels = new HashSet();
			    for (int i = 0; i < redisServer.length; i++) {
			    	 System.out.println(redisServer[i]);
					 sentinels.add(redisServer[i]);
			    } 
			    
			    GenericObjectPoolConfig config = new GenericObjectPoolConfig();
			    config.setMaxTotal(100);
			     //pool = new JedisSentinelPool(MASTER_NAME, sentinels, config, 2*1000);
			     pool = new JedisSentinelPool(MASTER_NAME, sentinels, config, 1000,null, 2);
			
			}

		} catch (Exception e) {
			System.out.println("Error init jedis:"+e.getMessage());
			e.printStackTrace();
		}
	}
	
	

	public static void main(String[] args) {

		while (true) {
			try {

				String key = generateRandomString(5);
				
				try {

					Jedis jc=getConnection();
					System.out.println(key + "=" + jc.set(key, "ok"));
					System.out.println("get" + "=" + jc.get(key));
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
				}
				System.out.println("ok");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
		}

	}

	private static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
	private static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();
	private static final String NUMBER = "0123456789";
	private static final String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER;
	private static SecureRandom random = new SecureRandom();

	public static String generateRandomString(int length) {
		if (length < 1)
			throw new IllegalArgumentException();

		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++) {

			// 0-62 (exclusive), random returns 0-61
			int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
			char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);

			// debug
			// System.out.format("%d\t:\t%c%n", rndCharAt, rndChar);

			sb.append(rndChar);

		}

		return sb.toString();

	}
	


           

	protected static Jedis getConnection()
	{               
	    if(pool==null)
	        initLocal();
	
	      return pool.getResource();     
	}  

}
