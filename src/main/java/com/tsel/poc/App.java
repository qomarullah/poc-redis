package com.tsel.poc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mashape.unirest.http.Unirest;
import com.tsel.routers.Router;
import com.tsel.utils.Configs;

public class App {
	
	public static Properties prop;
	public static String serverID;
	public static String serverVersion;
	public static String serverPort;
	
	public static Configs cf;
	public static int maxPool=200;
	public static String memcachedServer;
	public static String memcachedServerPool;
	public static String redisMaster;
	public static String redisServer;
	public static String redisConfig;
	public static String redisPassword;
	
	public static String kafkaLogServer;
	public static String kafkaLogTopic;
	private final static Logger log = LogManager.getLogger(App.class);
	public static boolean pinCache=false;
	public static int pinCacheDuration;
	public static boolean pinStore=false;
	public static String pinSalt;
	
	
	public static void main(String[] args) {
		// setup properties
		prop = new Properties();
		if (args.length > 0)
			loadProperties(prop, args[0]);
		else {
			log.debug("properties not found");
			System.exit(0);
		}

		// setup log
		if (args.length > 1)
			BasicConfigurator.configure();
			
		else
			PropertyConfigurator.configure(args[0]);

		// setup server
		int port = Integer.parseInt(prop.getProperty("server.port", "9001"));
		int maxThreads = Integer.parseInt(prop.getProperty("server.maxthread", "200"));
		int minThreads = Integer.parseInt(prop.getProperty("server.minthread", "30"));
		int timeOutMillis = Integer.parseInt(prop.getProperty("server.timeout", "20000"));
		serverID = prop.getProperty("server.id", "ag1");
		serverVersion = prop.getProperty("server.version", "1.0");
		serverPort = Integer.toString(port);
		
		//unirest
		Unirest.setConcurrency(App.maxPool, App.maxPool);

		//redis
		redisMaster = prop.getProperty("redis.master", "mymaster");
		redisServer = prop.getProperty("redis.server", "127.0.0.1:6379");
		redisConfig = prop.getProperty("redis.config", "10000|500|16|1000");
		redisPassword = prop.getProperty("redis.password", "");

		//kafkaLogServer = prop.getProperty("kafka.log.server", null);
		//kafkaLogTopic = prop.getProperty("kafka.log.topic", serverID);
		
		
		// start server
		new Router(port, minThreads, maxThreads, timeOutMillis, cf);
	}

	public static void loadProperties(Properties prop, String filename) {
		
		InputStream input = null;
		try {
			File tempFile = new File(filename);
			boolean exists = tempFile.exists();
			if (!exists) {
				System.out.println("config not found");
				System.exit(0);
			}
				
			input = new FileInputStream(filename);
			// load a properties file
			prop.load(input);
		} catch (IOException ex) {
			
			ex.printStackTrace();
		} finally {
			
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
