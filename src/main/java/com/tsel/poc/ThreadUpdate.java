package com.tsel.poc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.tsel.db.MysqlFacade;
import com.tsel.utils.LogTDR;
import com.tsel.utils.Utils;

public class ThreadUpdate extends TimerTask {
	
	protected static Logger logger = LogManager.getLogger(ThreadUpdate.class);
	   
	private volatile boolean isRunning = true;
	static ConcurrentHashMap<String, Long> hashLastUpdate = new ConcurrentHashMap<String, Long>();
	String serverid;
	
	public ThreadUpdate(String serverId) {
		super();
		logger.info("RefreshThread instance created...");
		this.serverid = serverId;
	}

	public void stop() {
		isRunning = false;
	}
	 
	
	 
	public void run() {
		for (; isRunning;) {

			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
			}
			
			logger.info("Check update config()");
			
		}
	}

}
