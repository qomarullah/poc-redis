package com.tsel.utils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLContext;

import org.apache.http.client.HttpClient;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.tsel.poc.App;
import com.tsel.routers.Router;

import spark.Request;

public class Utils {

	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
	private final static Logger log = LogManager.getLogger(Utils.class);

	public static void main(String[] args) throws UnsupportedEncodingException {
		// TODO Auto-generated method stub
		String key = "XXXXXXXX";
		String pin = "222222";
		String encrypt = encryptDES(key, pin);
		System.out.println("pin:" + pin);
		System.out.println("encrypt:" + encrypt);

		String decrypt = decryptDES(key, encrypt);
		System.out.println("decrypt:" + decrypt);

	}

	public Utils() {
	}

	public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
		Map<String, Object> retMap = new HashMap<String, Object>();

		if (json != JSONObject.NULL) {
			retMap = toMap(json);
		}
		return retMap;
	}

	public static Map<String, Object> toMap(JSONObject object) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();

		Iterator<String> keysItr = object.keys();
		while (keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key);

			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	public static List<Object> toList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}

	private static long counter = 10000;

	public static String generateTxid(String prefix, String msisdn) {
		SimpleDateFormat sdfTxid = new SimpleDateFormat("yyMMddHHmmss");
		String dt = sdfTxid.format(new Date());
		String suffix = msisdn.substring(msisdn.length() - 5, msisdn.length());
		synchronized (sdfTxid) {
			counter++;
			if (counter > 99999) {
				counter = 10000;
			}
		}
		return prefix + dt + suffix + String.valueOf(counter);
	}

	private static String transformationDES = "DESede/ECB/PKCS5Padding";
	private static SecretKeySpec secretKey;

	public static String encryptDES(String key, String str) throws UnsupportedEncodingException {
		String res = "";
		byte[] keyBytes = Base64.getDecoder().decode(key.getBytes(StandardCharsets.UTF_8));
		secretKey = new SecretKeySpec(keyBytes, "DESede");

		byte[] bytesenc = str.getBytes();
		byte[] bytes = encryptDes(bytesenc, secretKey);
		byte[] bytedecode = Base64.getEncoder().encode(bytes);
		res = new String(bytedecode, "UTF-8");

		return res;
	}

	public static String decryptDES(String key, String str) {
		String res = "";
		byte[] keyBytes = Base64.getDecoder().decode(key.getBytes(StandardCharsets.UTF_8));
		secretKey = new SecretKeySpec(keyBytes, "DESede");

		try {
			byte[] bytesenc = Base64.getDecoder().decode(str);
			byte[] bytes = decryptDes(bytesenc, secretKey);
			res = new String(bytes, "UTF-8");

		} catch (Exception e) {
			// TODO: handle exception
			// LogMain.error(e.getMessage());
		}
		return res;
	}

	public static byte[] encryptDes(byte[] raw, SecretKeySpec secretKey) throws CipherOperationException {
		try {
			Cipher cipher = Cipher.getInstance(transformationDES);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			return cipher.doFinal(raw);
		} catch (Exception e) {
			throw new CipherOperationException(e);
		}
	}

	public static byte[] decryptDes(byte[] encrypted, SecretKeySpec secretKey) throws CipherOperationException {

		try {
			Cipher cipher = Cipher.getInstance(transformationDES);
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			return cipher.doFinal(encrypted);
		} catch (Exception e) {
			throw new CipherOperationException(e);
		}
	}

	public static JSONObject xmlToJson(String xmlText, String rootField) {

		// String xmlText="<?xml version='1.0' encoding='UTF-8'?><soapenv:Envelope
		// xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"><soapenv:Body><api:Result
		// xmlns:api=\"http://cps.huawei.com/synccpsinterface/api_requestmgr\"
		// xmlns:com=\"http://cps.huawei.com/synccpsinterface/common\"
		// xmlns:res=\"http://cps.huawei.com/synccpsinterface/result\"><res:Header><res:Version>1.0</res:Version><res:OriginatorConversationID>p110001810031512315678910002</res:OriginatorConversationID><res:ConversationID>p110001810031512315678910002]</res:ConversationID></res:Header><res:Body><res:ResultType>0</res:ResultType><res:ResultCode>0</res:ResultCode><res:ResultDesc>Process
		// service request
		// successfully.</res:ResultDesc><res:PreValidationResult><res:UMC_SessionID>10100000000096007378</res:UMC_SessionID><res:ResultCode>0</res:ResultCode><res:ResultMsg>Tagihan
		// 03201810535591241872121531460 berjumlah Rp 210669.Masukkan PIN TCASH utk
		// pembayaran</res:ResultMsg><res:ChargeInfoList><res:ChargeInfo><res:IdentityBrief><res:IdentityId>202000000033375491</res:IdentityId><res:IdentityType>1000</res:IdentityType></res:IdentityBrief><res:Direction>D</res:Direction><res:AccountNo>100000000244592919</res:AccountNo><res:AccountTypeId>21025</res:AccountTypeId><res:Amount><res:Amount>0</res:Amount><res:UnitType>MINIMUM</res:UnitType><res:CurrencyCode>IDR</res:CurrencyCode></res:Amount><res:Ratio>1.0</res:Ratio><res:ChargeFeeType
		// /><res:ChargeTypeId>10001240</res:ChargeTypeId><res:ChargeProfileId>624</res:ChargeProfileId></res:ChargeInfo><res:ChargeInfo><res:IdentityBrief><res:IdentityId>200000000000010001</res:IdentityId><res:IdentityType>8000</res:IdentityType></res:IdentityBrief><res:Direction>C</res:Direction><res:AccountNo>800000000101010018</res:AccountNo><res:AccountTypeId>21140</res:AccountTypeId><res:Amount><res:Amount>0</res:Amount><res:UnitType>MINIMUM</res:UnitType><res:CurrencyCode>IDR</res:CurrencyCode></res:Amount><res:Ratio>1.0</res:Ratio><res:ChargeFeeType
		// /><res:ChargeTypeId>10001240</res:ChargeTypeId><res:ChargeProfileId>624</res:ChargeProfileId></res:ChargeInfo><res:ChargeInfo><res:IdentityBrief><res:IdentityId>200000000000010001</res:IdentityId><res:IdentityType>8000</res:IdentityType></res:IdentityBrief><res:Direction>D</res:Direction><res:AccountNo>800000000101001033</res:AccountNo><res:AccountTypeId>21036</res:AccountTypeId><res:Amount><res:Amount>0</res:Amount><res:UnitType>MINIMUM</res:UnitType><res:CurrencyCode>IDR</res:CurrencyCode></res:Amount><res:Ratio>1.0</res:Ratio><res:ChargeFeeType
		// /><res:ChargeTypeId>10001193</res:ChargeTypeId><res:ChargeProfileId>624</res:ChargeProfileId></res:ChargeInfo><res:ChargeInfo><res:IdentityBrief><res:IdentityId>202000000033375491</res:IdentityId><res:IdentityType>1000</res:IdentityType></res:IdentityBrief><res:Direction>C</res:Direction><res:AccountNo>100000000244592919</res:AccountNo><res:AccountTypeId>21025</res:AccountTypeId><res:Amount><res:Amount>0</res:Amount><res:UnitType>MINIMUM</res:UnitType><res:CurrencyCode>IDR</res:CurrencyCode></res:Amount><res:Ratio>1.0</res:Ratio><res:ChargeFeeType
		// /><res:ChargeTypeId>10001193</res:ChargeTypeId><res:ChargeProfileId>624</res:ChargeProfileId></res:ChargeInfo><res:ChargeInfo><res:IdentityBrief><res:IdentityId>200000000000010001</res:IdentityId><res:IdentityType>8000</res:IdentityType></res:IdentityBrief><res:Direction>D</res:Direction><res:AccountNo>800000000101010018</res:AccountNo><res:AccountTypeId>21140</res:AccountTypeId><res:Amount><res:Amount>0</res:Amount><res:UnitType>MINIMUM</res:UnitType><res:CurrencyCode>IDR</res:CurrencyCode></res:Amount><res:Ratio>1.0</res:Ratio><res:ChargeFeeType
		// /><res:ChargeTypeId>10001241</res:ChargeTypeId><res:ChargeProfileId>624</res:ChargeProfileId></res:ChargeInfo><res:ChargeInfo><res:IdentityBrief><res:IdentityId>201000000000232976</res:IdentityId><res:IdentityType>5000</res:IdentityType></res:IdentityBrief><res:Direction>C</res:Direction><res:AccountNo>500000000000002597</res:AccountNo><res:AccountTypeId>21029</res:AccountTypeId><res:Amount><res:Amount>0</res:Amount><res:UnitType>MINIMUM</res:UnitType><res:CurrencyCode>IDR</res:CurrencyCode></res:Amount><res:Ratio>1.0</res:Ratio><res:ChargeFeeType
		// /><res:ChargeTypeId>10001241</res:ChargeTypeId><res:ChargeProfileId>624</res:ChargeProfileId></res:ChargeInfo><res:ChargeInfo><res:IdentityBrief><res:IdentityId>200000000000010001</res:IdentityId><res:IdentityType>8000</res:IdentityType></res:IdentityBrief><res:Direction>D</res:Direction><res:AccountNo>800000000101001033</res:AccountNo><res:AccountTypeId>21036</res:AccountTypeId><res:Amount><res:Amount>15000</res:Amount><res:UnitType>MINIMUM</res:UnitType><res:CurrencyCode>IDR</res:CurrencyCode></res:Amount><res:Ratio>1.0</res:Ratio><res:ChargeFeeType
		// /><res:ChargeTypeId>10001194</res:ChargeTypeId><res:ChargeProfileId>624</res:ChargeProfileId></res:ChargeInfo><res:ChargeInfo><res:IdentityBrief><res:IdentityId>201000000000232976</res:IdentityId><res:IdentityType>5000</res:IdentityType></res:IdentityBrief><res:Direction>C</res:Direction><res:AccountNo>500000000000002597</res:AccountNo><res:AccountTypeId>21029</res:AccountTypeId><res:Amount><res:Amount>15000</res:Amount><res:UnitType>MINIMUM</res:UnitType><res:CurrencyCode>IDR</res:CurrencyCode></res:Amount><res:Ratio>1.0</res:Ratio><res:ChargeFeeType
		// /><res:ChargeTypeId>10001194</res:ChargeTypeId><res:ChargeProfileId>624</res:ChargeProfileId></res:ChargeInfo><res:ChargeInfo><res:IdentityBrief><res:IdentityId>201000000000232976</res:IdentityId><res:IdentityType>5000</res:IdentityType></res:IdentityBrief><res:Direction>D</res:Direction><res:AccountNo>500000000000002597</res:AccountNo><res:AccountTypeId>21029</res:AccountTypeId><res:Amount><res:Amount>0</res:Amount><res:UnitType>MINIMUM</res:UnitType><res:CurrencyCode>IDR</res:CurrencyCode></res:Amount><res:Ratio>1.0</res:Ratio><res:ChargeFeeType
		// /><res:ChargeTypeId>10001139</res:ChargeTypeId><res:ChargeProfileId>624</res:ChargeProfileId></res:ChargeInfo><res:ChargeInfo><res:IdentityBrief><res:IdentityId>200000000000010001</res:IdentityId><res:IdentityType>8000</res:IdentityType></res:IdentityBrief><res:Direction>C</res:Direction><res:AccountNo>800000000101001033</res:AccountNo><res:AccountTypeId>21036</res:AccountTypeId><res:Amount><res:Amount>0</res:Amount><res:UnitType>MINIMUM</res:UnitType><res:CurrencyCode>IDR</res:CurrencyCode></res:Amount><res:Ratio>1.0</res:Ratio><res:ChargeFeeType
		// /><res:ChargeTypeId>10001139</res:ChargeTypeId><res:ChargeProfileId>624</res:ChargeProfileId></res:ChargeInfo><res:ChargeInfo><res:IdentityBrief><res:IdentityId>202000000033375491</res:IdentityId><res:IdentityType>1000</res:IdentityType></res:IdentityBrief><res:Direction>D</res:Direction><res:AccountNo>100000000244592919</res:AccountNo><res:AccountTypeId>21025</res:AccountTypeId><res:Amount><res:Amount>250000</res:Amount><res:UnitType>MINIMUM</res:UnitType><res:CurrencyCode>IDR</res:CurrencyCode></res:Amount><res:Ratio>1.0</res:Ratio><res:ChargeFeeType
		// /><res:ChargeTypeId>10001156</res:ChargeTypeId><res:ChargeProfileId>624</res:ChargeProfileId></res:ChargeInfo><res:ChargeInfo><res:IdentityBrief><res:IdentityId>200000000000010001</res:IdentityId><res:IdentityType>8000</res:IdentityType></res:IdentityBrief><res:Direction>C</res:Direction><res:AccountNo>800000000101001033</res:AccountNo><res:AccountTypeId>21036</res:AccountTypeId><res:Amount><res:Amount>250000</res:Amount><res:UnitType>MINIMUM</res:UnitType><res:CurrencyCode>IDR</res:CurrencyCode></res:Amount><res:Ratio>1.0</res:Ratio><res:ChargeFeeType
		// /><res:ChargeTypeId>10001156</res:ChargeTypeId><res:ChargeProfileId>624</res:ChargeProfileId></res:ChargeInfo></res:ChargeInfoList></res:PreValidationResult><res:ReferenceData><res:ReferenceItem><com:Key>bill_ref</com:Key><com:Value>03201810535591241872121531460</com:Value></res:ReferenceItem><res:ReferenceItem><com:Key>amount</com:Key><com:Value>210669</com:Value></res:ReferenceItem><res:ReferenceItem><com:Key>BillerName</com:Key><com:Value>Tagihan
		// Listrik</com:Value></res:ReferenceItem></res:ReferenceData></res:Body></api:Result></soapenv:Body></soapenv:Envelope>";
		// xmlText=xmlText.replaceAll("res:","");
		String openField = "<" + rootField + ">";
		String closeField = "</" + rootField + ">";
		int first = xmlText.indexOf(openField);
		int second = xmlText.indexOf(closeField) + closeField.length();

		xmlText = xmlText.substring(first, second);
		JSONObject xmlJSONObj = new JSONObject();
		// String jsonPrettyPrintString="";

		try {
			xmlJSONObj = XML.toJSONObject(xmlText);
			/*
			 * if(type!=null && type.equals("pretty")) { jsonPrettyPrintString =
			 * xmlJSONObj.toString(4); }else { jsonPrettyPrintString=xmlJSONObj.toString();
			 * }
			 */
		} catch (JSONException je) {
			log.error("error:", je.toString());
		}
		return xmlJSONObj;
	}

	public static JSONObject mergeJSONObjects(JSONObject json1, JSONObject json2) {
		JSONObject mergedJSON = new JSONObject();
		try {
			mergedJSON = new JSONObject(json1, JSONObject.getNames(json1));
			for (String crunchifyKey : JSONObject.getNames(json2)) {
				mergedJSON.put(crunchifyKey, json2.get(crunchifyKey));
			}

		} catch (JSONException e) {
			throw new RuntimeException("JSON Exception" + e);
		}
		return mergedJSON;
	}

	public static HttpClient makeClient() {
		SSLContextBuilder builder = new SSLContextBuilder();
		CloseableHttpClient httpclient = null;
		try {
			// builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			builder.loadTrustMaterial(null, new TrustStrategy() {
				@Override
				public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
					// TODO Auto-generated method stub
					return true;
				}
			});
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());

			httpclient = HttpClients.custom().setSSLSocketFactory(sslsf)
					.setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
			// System.out.println("custom httpclient called");
			// System.out.println(httpclient);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		return httpclient;
	}

	private static HttpClient unsafeHttpClient;
	static {
		try {
			SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy() {
				public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
					return true;
				}
			}).build();

			unsafeHttpClient = HttpClients.custom().setSSLContext(sslContext)
					.setSSLHostnameVerifier(new NoopHostnameVerifier()).build();

		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			e.printStackTrace();
		}
	}

	public static HttpClient getClient() {
		return unsafeHttpClient;
	}


	public static void writeTDR(long start, Request request, String result, String err) {

		//CompletableFuture.runAsync(() -> {
			
			HashMap<String, Object> logData = new HashMap<String, Object>();
			HashMap<String, Object> addData = new HashMap<String, Object>();
			Object logDataJson = "";

			try {
				HashMap<String, Object> headers = new HashMap<>();
				for (String s : request.headers()) {
					headers.put(s, request.headers(s));
				}
				long rt = System.currentTimeMillis() - start;
				logData = new HashMap<String, Object>();
				addData = new HashMap<String, Object>();
				logData.put("logTime", dateFormat.format(new Date()));
				logData.put("app", App.serverID);
				logData.put("ver", App.serverVersion);
				logData.put("port", App.serverPort);
				logData.put("srcIP", request.ip());
				logData.put("rt", rt);
				logData.put("path", request.pathInfo());
				logData.put("header", headers);
				
				//get body
				/*String bodyString=request.body().replaceAll("\n", "").replaceAll("\t", "");
				Map<String, String> body=null;
				Map<String, String> resp=null;
				try {
					ObjectMapper mapper = new ObjectMapper();
					body = mapper.readValue(bodyString, new TypeReference<Map<String, Object>>() {
					});
					body.remove("xpin");
					resp = mapper.readValue(result, new TypeReference<Map<String, Object>>() {
					});
					//remove for inconsistency log
					resp.remove("data");
					
				} catch (Exception e) {
					 e.printStackTrace();
				}
				*/
				
				logData.put("req", request.body());
				logData.put("resp", result);
				logData.put("err", err);

				// kafka
				if (App.kafkaLogServer != null) {
					boolean kafkaResp = false;
					MyKafkaProducer producer = null;
					try {
						producer = MyKafkaProducer.getKafka(App.kafkaLogServer);
						kafkaResp = producer.send(App.kafkaLogTopic, null, logDataJson.toString());
					} catch (Exception e) {
						e.printStackTrace();
						log.error("kafkaf-error", e.getMessage());
					}
					addData.put("kafkaLogServer", App.kafkaLogServer);
					addData.put("kafkaLogTopic", App.kafkaLogTopic);
					addData.put("kafkaLogResp", kafkaResp);
				}

				logData.put("addData", addData);
				ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
				logDataJson = objectMapper.writeValueAsString(logData);

			} catch (JsonProcessingException e) {
				e.printStackTrace();
				logData.put("err", e.getMessage());

			}
			String tdr = logDataJson.toString();
			LogTDR.info(tdr);

		//});

	}

}
