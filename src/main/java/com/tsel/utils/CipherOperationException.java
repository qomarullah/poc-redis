package com.tsel.utils;

/**
 * 
 * @author zakyalvan
 * @since 13 Jan 2017
 */
@SuppressWarnings("serial")
public class CipherOperationException extends RuntimeException {

	public CipherOperationException(String message, Throwable cause) {
		super(message, cause);
	}

	public CipherOperationException(String message) {
		super(message);
	}

	public CipherOperationException(Throwable cause) {
		super(cause);
	}
	
}
