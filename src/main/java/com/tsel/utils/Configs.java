package com.tsel.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.tsel.models.Result;

public class Configs {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public static HashMap<String, String> clients = new HashMap<String, String>();
	public static HashMap<String, String> files = new HashMap<String, String>();
	
	
	protected static Logger log = LogManager.getLogger(Configs.class);
	
	public boolean loadFiles(String prefix, String path) {
		File tempFile = new File(path);
		boolean exists = tempFile.exists();
		if (!exists) {
			System.out.println("payload not found");
			System.exit(0);
		}
		
		
		
		boolean resp=true;
		final File folder = new File(path);
		log.debug("Path = "+path);
		for (final File fileEntry : folder.listFiles()) {
			if (!fileEntry.isDirectory()) {
	        		String filename=path+"/"+fileEntry.getName();
	        		String content=readFile(filename);
	        		files.put(prefix+"."+fileEntry.getName(), content);
	        		//log.debug(prefix+"."+fileEntry.getName()+" = "+content);	        		
	        	}
	        
	    }
		return resp;
	}

	public String readFile(String filename) {
		String resp="";
		BufferedReader br = null;
		FileReader fr = null;
		try {
			fr = new FileReader(filename);
			br = new BufferedReader(fr);
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				resp+=sCurrentLine;
			}
		} catch (IOException e) {
			e.printStackTrace();

		} finally {
			try {
				if (br != null)
					br.close();
				if (fr != null)
					fr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return resp;
	}
}
