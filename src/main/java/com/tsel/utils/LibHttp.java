package com.tsel.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.utils.StringUtils;

import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.io.OutputStreamWriter;
public class LibHttp {

	
	
	protected static Logger logger = LogManager.getLogger(LibHttp.class);
	
	public String[] get(String url, int timeout) {
		return get(url, timeout, null,null,null,null);
	}
	

	public String[] get(String url, int timeout, String proxyHost,
			String proxyPort, String headerKey, String headerValue) {
		long start = System.nanoTime();
		url = url.replaceAll(" ", "+");
		String[] response = new String[2];
		
		HttpClient http = new HttpClient();
		
		http.getParams().setSoTimeout(timeout);
		if (proxyHost != null && proxyPort != null) {
			logger.info("Using proxy = " + proxyHost + ":" + proxyPort);
			http.getHostConfiguration().setProxy(proxyHost,
					Integer.parseInt(proxyPort));
		}
		HttpMethod method = new GetMethod(url);
		http.getParams().setParameter("http.socket.timeout", timeout);
		http.getParams().setParameter("http.connection.timeout", timeout);
		method.getParams().setParameter("http.socket.timeout", timeout);
		method.getParams().setParameter("http.connection.timeout", timeout);
	

		try {
			int statusCode = http.executeMethod(method);
			if (statusCode != HttpStatus.SC_OK && statusCode != 202) {
				response[0] = "HTTP_FAILED";
				response[1] = method.getResponseBodyAsString().trim();
			} else {
				response[0] = method.getResponseBodyAsString().trim();
			
			}
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("Error in httpGet", e);
			response[0] = "FAILED";
		} finally {
			method.releaseConnection();
		}
		
		LogTDR.debug("HTTP-GET|" + url + "|Response|" + response + "|"+ (System.nanoTime() - start) + "ms");

		return response;
	}




	 public String post(String url, String remFile, int timeout, String contentType, String headerKey, String headerValue){

			String respXML="";
			String PN = "HTTP-REQUEST";
			String inputLine;
			StringBuffer resp = new StringBuffer();
			
			try{

				URL obj = new URL(url);
				HttpURLConnection con = (HttpURLConnection) obj.openConnection();
				con.setRequestMethod("POST");
				if (StringUtils.isNotEmpty(contentType) && contentType.contains("json")){
					con.setRequestProperty("Content-Type","application/json;charset=UTF-8");
				} else {
					con.setRequestProperty("Content-Type","text/xml;charset=UTF-8");
				}
				if (StringUtils.isNotEmpty(headerKey)) {
					String[] key = headerKey.split("\\|\\|");
					if (key.length > 0) {
						String[] value = headerValue.split("\\|\\|");
						for (int i = 0 ; i < key.length ; i ++){
							con.setRequestProperty(key[i],value[i]);
						}
					}
				}
				con.setDoOutput(true);
			
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				wr.writeBytes(remFile);
				wr.flush();
				wr.close();
				
				int responseCode = con.getResponseCode();
				//LogTDR.debug("\nSending 'POST' request to URL : " + url);
				//LogTDR.debug("Response Code : " + responseCode);
				if (con.getResponseCode() >= 400) {
					//    is = httpConn.getErrorStrieam();
					BufferedReader in2 = new BufferedReader(new InputStreamReader(con.getErrorStream()));
					while ((inputLine = in2.readLine()) != null) {
					        resp.append(inputLine);
					}
					in2.close();
				
					//print result
					logger.info(PN + "|HTTP|POST(" + url + "," + remFile + "," + timeout + ")=" + resp.toString());
				
				} else {
				    //is = httpConn.getInputStream();
					BufferedReader in2 = new BufferedReader(new InputStreamReader(con.getInputStream()));
					while ((inputLine = in2.readLine()) != null) {
					        resp.append(inputLine);
					}
					in2.close();
				}
				
				
				logger.info(PN + "|HTTP|POST(" + url + "," + remFile + "," + timeout + ")=" + resp.toString());
				LogTDR.debug(PN + "|HTTP|POST(" + url + "," + remFile + "," + timeout + ")=" + resp.toString());
				
				respXML=resp.toString();
				return respXML;
			
				
		} catch(Exception i)
		{
	      logger.fatal(PN + "|HTTP|POST(" + url + "," + remFile + "," + timeout + ")=" + i.getMessage(), i);
	      return "FAILED" ;

	  	}
	 }

	public String post(String url, String remFile, int timeout){
		return post(url, remFile, timeout, null, null, null);
	}
	

	 public String httpsPost(String url, String remFile, int timeout, String contentType, String headerKey, String headerValue){

			String respXML="";
			String PN = "HTTP-REQUEST";
			String inputLine;
			StringBuffer resp = new StringBuffer();
			
			try{
				
			    TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
			        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			            return null;
			        }
			        public void checkClientTrusted(X509Certificate[] certs, String authType) {
			        }
			        public void checkServerTrusted(X509Certificate[] certs, String authType) {
			        }
			    }
			};
			
			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			
			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
			    public boolean verify(String hostname, SSLSession session) {
			        return true;
			    }
			};
			
			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

		    URL obj = new URL(url);
		    HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
		    
		    if (StringUtils.isNotEmpty(contentType) && contentType.contains("json")){
		    		conn.setRequestProperty("Content-Type","application/json;charset=UTF-8");
			} else {
				conn.setRequestProperty("Content-Type","text/xml;charset=UTF-8");
			}
			if (StringUtils.isNotEmpty(headerKey)) {
				String[] key = headerKey.split("\\|\\|");
				if (key.length > 0) {
					String[] value = headerValue.split("\\|\\|");
					for (int i = 0 ; i < key.length ; i ++){
						conn.setRequestProperty(key[i],value[i]);
					}
				}
			}
			
		    //conn.setHostnameVerifier(allHostsValid);
		    conn.setDoOutput(true);
		 
		    conn.setRequestMethod("POST");
		 
		    //String userpass = "user" + ":" + "pass";
		    //String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes("UTF-8"));
		    //conn.setRequestProperty ("Authorization", basicAuth);
		 
		    OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
		    out.write(remFile);
		    out.close();
				
				int responseCode = conn.getResponseCode();
				if (conn.getResponseCode() >= 400) {
					// is = httpConn.getErrorStrieam();
					BufferedReader in2 = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
					while ((inputLine = in2.readLine()) != null) {
					        resp.append(inputLine);
					}
					in2.close();
				
					//print result
					logger.info(PN + "|HTTP|POST(" + url + "," + remFile + "," + timeout + ")=" + resp.toString());
				
				} else {
				    //is = httpConn.getInputStream();
					BufferedReader in2 = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					while ((inputLine = in2.readLine()) != null) {
					        resp.append(inputLine);
					}
					in2.close();
				}
				
				
				logger.info(PN + "|HTTP|POST(" + url + "," + remFile + "," + timeout + ")=" + resp.toString());
				LogTDR.debug(PN + "|HTTP|POST(" + url + "," + remFile + "," + timeout + ")=" + resp.toString());
				
				respXML=resp.toString();
				return respXML;
			
				
		} catch(Exception i)
		{
	      logger.fatal(PN + "|HTTP|POST(" + url + "," + remFile + "," + timeout + ")=" + i.getMessage(), i);
	      return "FAILED" ;

	  	}
	 }
}
