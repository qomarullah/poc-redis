package com.tsel.utils;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Configure the Kafka consumer as defined in the <strong>config.location</strong> yaml file.  Expose this
 * Kafka producer as a singleton.
 */
public class MyKafkaProducer {
    final static Logger log = LogManager.getLogger(MyKafkaProducer.class);
    
    private static MyKafkaProducer instance;
    private static Object lock = new Object();

    private static ConcurrentHashMap<String, MyKafkaProducer> instances = new ConcurrentHashMap<String, MyKafkaProducer>();
	
    //private KafkaProducer producer;
    private KafkaProducer kafka;
	
    private String kafkaServer;
    /**
     * Configure the Kafka consumer as defined in the <strong>config.location</strong> yaml file.  Expose this
     * Kafka producer as a singleton.
     */
    private MyKafkaProducer(String kafkaServer) {
        //
        // Setup Kafka producer
        //
        // http://kafka.apache.org/082/documentation.html#producerconfigs
        /*Properties props = new Properties();
        props.put("bootstrap.servers", Configuration.get().getKafka().bootstrapServers);
        for (String key : Configuration.get().getKafkaProducer().properties.keySet())
            props.put(key, Configuration.get().getKafkaProducer().properties.get(key));
        producer = new org.apache.kafka.clients.producer.KafkaProducer(props);
        */
    	Properties properties = new Properties();
		properties.put("bootstrap.servers", kafkaServer);
		//properties.put("client.id", clientId);
		properties.put("acks", "all");
		//properties.put("message.timeout.ms", "1000");
		//properties.put("request.timeout.ms", "1000");
		//properties.put("transaction.timeout.ms", "1000");
		//properties.put("auto.commit.interval.ms", "1000");
		
		
		properties.put("retries", "0");
		properties.put("batch.size", "16384");
		properties.put("linger.ms", "0");
		properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		//properties.put("block.on.buffer.full", "true");
		kafka = new KafkaProducer<>(properties);
		
    }

    /**
     * Return the instance of this singleton.
     * @return
     */
   /* public static MyKafkaProducer get(String kafkaServer) {
        if (instance == null) instance = new MyKafkaProducer(kafkaServer);
        return instance;
    }
    */
    public static MyKafkaProducer getKafka(String kafkaServer) throws Exception {
    		MyKafkaProducer instance = instances.get(kafkaServer);
		if (instance == null) {
			log.debug("new connection producer.......");
			synchronized (lock) {
				if (instances.get(kafkaServer) == null) {
					instance = new MyKafkaProducer(kafkaServer);
					instances.put(kafkaServer, instance);
				} else {
					instance = instances.get(kafkaServer);
				}
			}
		}
		if (instance == null) {
			return null;
		}
		long ts = System.currentTimeMillis();
		//kafka = instance.kafka
		log.debug("Thread " + Thread.currentThread().getId()
				+ " getConnection() " + kafkaServer + " = "
				+ (System.currentTimeMillis() - ts) + " ms");
		return instance;
	}

    public static class MyCallback implements Callback {
        public boolean success = true;
        @Override
        public void onCompletion(RecordMetadata recordMetadata, Exception e) {
            if(e != null) {
            		log.error("Unable to publish message to kafka.", e);
                success = false;
            } else
            		log.debug("The offset of the record we just sent is: " + recordMetadata.offset());
        }
    }

    /**
     * Send the message to the Kafka topic.
     *
     * @param topic
     * @param key
     * @param message
     * @return true if the message was successfully sent, false if not.
     */
    public boolean send(String topic, String key, String message) {
        MyCallback myCallback = new MyCallback();
        //ProducerRecord<String, String> record = new ProducerRecord<>(topic, key, message);
        ProducerRecord<String, Object> record = new ProducerRecord<>(topic, message);
        
        Future<RecordMetadata> response = kafka.send(record, myCallback);
        // Wait her until we get a response.
        try {
            response.get();
        } catch (Exception e) {
            log.error(e);
            return false;
        }
        return myCallback.success;
    }
}