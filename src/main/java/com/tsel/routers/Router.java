package com.tsel.routers;

/**
 * Bismillahirrahmanirrahim
 * @author Qomarullah
 * @time 9:27:44 PM
 */

import static spark.Spark.*;

import java.time.Duration;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tsel.controllers.Cache;
import com.tsel.models.Result;
import com.tsel.utils.Configs;
import com.tsel.utils.Utils;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Timer;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Spark;

public class Router {

	private final static Logger log = LogManager.getLogger(Router.class);
	private static final HashMap<String, String> corsHeaders = new HashMap<String, String>();
	static {
		corsHeaders.put("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
		corsHeaders.put("Access-Control-Allow-Origin", "*");
		corsHeaders.put("Access-Control-Allow-Headers",
				"Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,");
		corsHeaders.put("Access-Control-Allow-Credentials", "true");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}
	public Configs config;
	
	public final static void apply() {
		Filter filter = new Filter() {
			@Override
			public void handle(Request request, Response response) throws Exception {
				corsHeaders.forEach((key, value) -> {
					//response.header(key, value);
				});
				response.type("application/json");
			}
		};
		Spark.after(filter);
	}
	
 
 
	public Router(int port, int minThreads, int maxThreads, int timeOutMillis, Configs config){
		PrometheusMeterRegistry prometheusRegistry = new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);
		//CompositeMeterRegistry compositeRegistry = new CompositeMeterRegistry();
		
		Counter counter = Counter
				  .builder("instance")
				  .description("indicates instance count of the object")
				  .tags("app", "counter")
				  .register(prometheusRegistry);
		
		Timer timer1 = prometheusRegistry.timer("app.delay1");
		Timer timer2 = prometheusRegistry.timer("app.delay2");
		Timer timer11 = prometheusRegistry.timer("app.redis-delay1");
		Timer timer22 = prometheusRegistry.timer("app.redis-delay2");
		
		
		this.config = config;
		Spark.port(port);
		threadPool(maxThreads, minThreads, timeOutMillis);
		Spark.get("/status", (req, res) -> "OK");
		
		Spark.get("/metrics", (request, response) -> {
			String resp = prometheusRegistry.scrape(); 
			return resp;
		});
		
		Spark.get("/redis1", (request, response) -> {
			
			//counter.increment(1);
			long start=System.currentTimeMillis();
			String router = request.pathInfo();
			log.debug("router:"+router);
			Duration rt=Duration.ofMillis(System.currentTimeMillis()-start);
			timer1.record(rt);
			
			return handleCache(request, response, timer11); 
		});
		
		Spark.get("/redis2", (request, response) -> {
			
			//counter.increment(1);
			long start=System.currentTimeMillis();
			String router = request.pathInfo();
			log.debug("router:"+router);
			Duration rt=Duration.ofMillis(System.currentTimeMillis()-start);
			timer2.record(rt);
			
			return handleCache(request, response, timer22); 
		});
		
		
		
		apply();
	}
	
	public String handleCache(Request request, Response response, Timer timer) throws JsonProcessingException {

		long start=System.currentTimeMillis();
		String resp="";
		String err="";
		
		//check auth client
		//String client_id=request.headers("client-id");
		//String x_api_key=request.headers("x-api-key");
	
		Result result=null;
		Cache cache = new Cache();
			
		String type=request.queryParamOrDefault("type","set");
		String key=request.queryParamOrDefault("key", "hello");
		
		log.debug("type:"+type+","+key);
		
		if(type.equals("remove")) {
			result=cache.cacheRemove(key);
		}
		if(type.equals("get")) {
			result=cache.cacheGet(key);
		}

		if(type.equals("set")) {
			String value=request.queryParams("value");
			int duration=Integer.parseInt(request.queryParamOrDefault("duration","0"));	
			result=cache.cacheSet(key,value,duration);		
		}
		
		if(type.equals("setSize")) {
			String value=request.queryParams("value");
			int duration=Integer.parseInt(request.queryParamOrDefault("duration","0"));	
			result=cache.cacheSetSize(key,value,duration,timer);		
		}
		if(type.equals("setSizeCompress")) {
			String value=request.queryParams("value");
			int duration=Integer.parseInt(request.queryParamOrDefault("duration","0"));	
			result=cache.cacheSetSizeCompress(key,value,duration,timer);		
		}
		
		if(type.equals("flush")) {
			result=cache.cacheFlush();
		}
		
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put("message", result.getMessage());
		jsonResponse.put("status", result.getStatus());
		jsonResponse.put("data", result.getData());
		resp = jsonResponse.toString();
		
		Utils.writeTDR(start, request, resp, err);
	    return resp;
 }
	
}